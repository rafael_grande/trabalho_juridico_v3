class AddJobsTags < ActiveRecord::Migration
    def self.up
        create_table :jobs_tags do |t|
            t.references :job, :tag
        end
  end

  def self.down
    drop_table :jobs_tags
  end

end
