class ChangeColumnName < ActiveRecord::Migration
  def change
      rename_column :jobs, :type, :type_job
  end
end
