class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :title
      t.string :type
      t.text :description
      t.string :value

      t.timestamps null: false
    end
  end
end
