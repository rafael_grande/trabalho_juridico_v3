class AddCompanyIdToJobs < ActiveRecord::Migration
    def change
        rename_column :jobs, :user_id, :company_id
    end
end
