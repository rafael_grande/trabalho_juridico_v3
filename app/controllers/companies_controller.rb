class CompaniesController < ApplicationController
    def index
        @companies = Company.all
    end
    
    def new
        @company = Company.new
    end
    
    def create
        @company = Company.new(company_params)
        @company.user_id = current_user.id
        if @company.save
            redirect_to @company, notice: "Vaga cadastrada"
        else
            render 'new'
        end
    end
    
    private
    
    def company_params
        params.require(:company).permit(:name, :image, :number_telephone, :description)
    end

    
end
