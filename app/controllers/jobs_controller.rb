class JobsController < ApplicationController
    before_action :find_job, only: [:show, :edit, :update, :destroy]
    
    def index
        @jobs = Job.all.order("created_at DESC")
    end
    
    def new
        @job = Job.new
    end

    def create
        @job = Job.new(job_params)
        @job.company_id = current_user.company.id
        
        if @job.save
            redirect_to @job, notice: "Vaga cadastrada"
        else
            render 'new'
        end
    end
    
    def edit
    end
    
    def update
        if @job.update(job_params)
            redirect_to @job, notice: "Vaga Atualizada"
        else
            render 'edit'
        end
    end
    
    def destroy
        @job.destroy
        redirect_to root_path
    end
    
    def show
    end
    
    private
    def job_params
        params.require(:job).permit(:title, :type_job, :description, :value, {:tag_ids => []})
    end
    
    
    
    def find_job
        @job = Job.find(params[:id])
    end
end
