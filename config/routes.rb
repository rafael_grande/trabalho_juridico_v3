Rails.application.routes.draw do
  resources :tags
    devise_for :users
    
    resources :jobs, :companies
    root "jobs#index"
end
